var tl;
(function($) {
    $(window).load(function() {
        var eventSource = new Timeline.DefaultEventSource();
	// Initialize the Timeline library in the DIV "my-timeline"
	var bandInfos = [
		Timeline.createBandInfo({
		    "width": "90%",
		    "intervalUnit": Timeline.DateTime.MONTH,
                    "intervalPixels": 80,
                    "eventSource": eventSource,
		   "layout": 'original'  // original, overview, detailed
		}),
		Timeline.createBandInfo({
                    "overview": true,
		    "width": "10%",
		    "intervalUnit": Timeline.DateTime.YEAR,
		    "intervalPixels": 960,
                    "eventSource": eventSource,
		    "layout": 'original'  // original, overview, detailed
		})
	    ];
	bandInfos[1].syncWith = 0;
	bandInfos[1].highlight = true;
	// Display the Timeline
	tl = Timeline.create(document.getElementById("my-timeline"), bandInfos);
        var url = '.'; // The base url for image, icon and background image
                       // references in the data
        eventSource.loadJSON(Drupal.settings.json_data, url);
        tl.layout(); // display the Timeline
    });
	// When the window is resized, delay half a second before refreshing the
	// Timeline to prevent rapid resize actions from slowing the browser.
        var resizeTimerID = null;
	$(window).resize(function() {
	    if (resizeTimerID == null) {
		resizeTimerID = setTimeout(function() {
		    resizeTimerID = null;
		    tl.layout();
		}, 500);
	    }
	});
})(jQuery);
